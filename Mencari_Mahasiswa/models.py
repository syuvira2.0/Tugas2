from django.db import models

# Create your models here.
class Mahasiswa(models.Model):
    name = models.CharField(max_length=500)
    npm = models.CharField(max_length=100)
    angkatan = models.CharField(max_length=400)
    keahlian = models.CharField(max_length=400)
    profile = models.ForeignKey