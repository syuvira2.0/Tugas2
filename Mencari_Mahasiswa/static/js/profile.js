$(document).ready(() => {
    $('#edit-profile').click(enterEditingMode)
    $('#edit-finish').click(exitEditingMode)
    $('#submit-skill').click(buttonPostSkill)
    $('#skill-title').popover('disable')
    $('.linkedin').click(() => linkedinLogin())
    if (editingFromStart) enterEditingMode()
})

const linkedinLogin = function(){
    console.log('Attempting to connect with linkedin..')
    IN.User.authorize(() => {
        IN.API.Profile("me").fields("first-name", "last-name", "email-address", "picture-url", "headline", "positions", "public-profile-url")
            .result(data => {
                const linkedin = {
                    email: data.values[0].emailAddress,
                    name: data.values[0].firstName + ' ' + data.values[0].lastName,
                    headline: data.values[0].headline,
                    pic: data.values[0].pictureUrl,
                    url: data.values[0].publicProfileUrl,
                }
                console.log(linkedin)
                updateProfile(linkedin)
            })
            .error(error => {
                alert('Gagal mengubungkan dengan akun Linkedin, kami mohon maaf atas ketidaknyamanan ini.')
                console.log(error)
            })
    })
}

const updateProfile = function(linkedin){
    $('.user-name').html(linkedin.name)
    $('.user-headline').html(linkedin.headline)
    $('.user-email').html(linkedin.email)
    $('#linkedin-url').html('<a href="'+ linkedin.url +'">'+ linkedin.url +'</a>')
    $('.linkedin').remove()
    $('.kosong').remove()
    $('.profile-pic-container').hide()
    $('.profile-pic-container').html('<img class="rounded-circle profile-pic mt-2" src="'+ linkedin.pic +'">')
    $('.profile-pic-container').show('slow')
    linkedin.csrfmiddlewaretoken = csrfToken
    $.ajax({
        url: 'linkedin/', method: 'POST',
        data: linkedin,
        success: response => console.log('berhasil update pada database :)'),
        error: error => console.log('Gagal update pada database :( ' + error)
    })
}

const enterEditingMode = function(){
    $('#edit-finish').show('normal')
    $('.delete-skill').show('normal')
    $('.linkedin').show('normal')
    $('.nilai').show('normal')
    $('.add-skill').show('normal')
    $('#edit-profile').hide('normal')
    $('.kosong').hide('normal')
}

const exitEditingMode = function(){
    $('#edit-finish').hide('normal')
    $('.delete-skill').hide('normal')
    $('.linkedin').hide('normal')
    $('.nilai').hide('normal')
    $('.add-skill').hide('normal')
    $('#edit-profile').show('normal')
    $('.kosong').show('normal')
}


const toggleNilai = function(isShowing){
    var toggle = 0
    if (isShowing) toggle = 1
    console.log('mencoba menampilkan/menyembunyikan nilai..')
    $.ajax({
        url: 'nilai/', method: 'POST',
        data: {
            show: toggle,
            csrfmiddlewaretoken: csrfToken
        },
        success: response => console.log('berhasil update pada database :)'),
        error: error => console.log('Gagal update pada database :( ' + error)
    })
}

const buttonPostSkill = function(){
    if ($('#skill-title').val().length < 2 || $('#skill-title').val().length > 16) {
        $('#skill-title').popover('enable')
        $('#skill-title').popover('show')
        console.log('invalid')
        return
    }
    $('#skill-title').popover('hide')
    $('#skill-title').popover('disable')
    $('#submit-skill').text('Menambahkan...')
    $('#submit-skill').addClass('disabled')
    postSkill($('#skill-title').val(), $('#skill-level').val(), id => {
        $('#submit-skill').text('Tambahkan')
        $('#submit-skill').removeClass('disabled')
        addSkill($('#skill-title').val(), $('#skill-level').val(), id)
        if ($('#skill-kosong')) $('#skill-kosong').hide('normal')
        $('#skill-title').val('')
    }, () => alert('Koneksi bermasalah'))
}

const addSkill = function(title, level, id){
    var badge = 'success'
    if (level == 'A') {
        badge = 'info'
        level = 'Advanced'
    } else if (level == 'E') {
        badge = 'primary'
        level = 'Expert'
    } else if (level =='L') {
        badge = 'danger'
        level = 'Legend'
    } else level = 'Intermediate'
    skill = $('<div class="row ml-1 mb-2" id="delete-skill-'+ id +'">'+ title +' <span ' +
    'class="badge badge-'+ badge +' ml-1">'+
    level + '</span>'+
    '<button type="button" class="btn btn-light delete-skill btn-sm ml-2" onclick="buttonDeleteSkill($(\'#delete-skill-'+id+'\'), '+id+')" ><i class="fa fa-times" aria-hidden="true"></i></button>' +
    '</div>()')
    skill.hide()
    $('#skill-list').append(skill)
    skill.show('normal')
}

const buttonDeleteSkill = function(skillButton, skillId){
    console.log('deleting skill ' + skillId)
    deleteSkill(skillId, () => {
        skillButton.hide('slow')
    }, () => alert('Koneksi bermasalah..'))
}

const postSkill = function(title, level, onSuccess, onError){
    $.ajax({
        url: 'api/skills/', method: 'POST',
        data: {
            title:title, 
            level:level, 
            csrfmiddlewaretoken: csrfToken
        },
        success: response => onSuccess(response.id),
        error: error => onError()
    })
}

const deleteSkill = function(id, onSuccess, onError){
    $.ajax({
        url: 'api/skills/'+ id +'/', 
        method: 'DELETE', 
        success: response => onSuccess(),
        error: error => onError(),
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrfToken);
        },
    })
}

