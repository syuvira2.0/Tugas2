from django.shortcuts import render
from .models import Mahasiswa
from login_status.utils import require_login, get_logged_in_user_npm
from profile_page.models import Profile,Skill
#from login_status.utils import get_logged_in_user_npm
# Create your views here.
response={}
def index(request):
    html='Mencari_Mahasiswa/mencari_mahasiswa.html'
    mahasiswa_list = Profile.objects.all()
    mahasiswa_keahlian =Skill.objects.all()
    response['mahasiswa_list']=mahasiswa_list
    response['mahasiswa_keahlian']=mahasiswa_keahlian
    return render(request, html, response)

