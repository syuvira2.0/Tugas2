CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *
PIPELINES STATUS : [![pipeline status](https://gitlab.com/syuvira2.0/Tugas2/badges/master/pipeline.svg)](https://gitlab.com/syuvira2.0/Tugas2/commits/master)


COVERAGE REPORT : [![coverage report](https://gitlab.com/syuvira2.0/Tugas2/badges/master/coverage.svg)](https://gitlab.com/syuvira2.0/Tugas2/commits/master)



ASSIGNMENT (project 2)

GROUP NAME : SYUVIRA

GROUP MEMBERS:
1. Tarsvini Ravinther
2. Rossalmira Abdul Rahman
3. Syuhadah Binti Muhamed Sa'ad

			 
Heroku Apps:
 Project 1 : (https://syuvira.herokuapp.com/)
 
 Project 2 : (https://syuvira-2017.herokuapp.com/)

            
			 "There are no secrets to success. It is the result of preparation, hard work, and learning from failure."-- Colin Powell