# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve, reverse
# from django.http import HttpRequest
# from .views import index, add_status, delete_status
# from profile_page.models import Status, Profile
# from profile_page import get_profile_by_npm
# from .forms import Status_Form
# import base64
# import os
# from login_status.apis import csui
# from login_status import custom_auth

# # Create your tests here.

# class UpdateStatusUnitTest(TestCase):

    # def test_login_page_logged_in(self):
        # self.__login()
        # r = self.client.get(reverse('login-status:index'))
        # self.assertEqual(r.url, reverse('index'))

    # def test_status_page(self):
        # self.__login()
        # r = self.client.get(reverse('login-status:status'))
        # self.assertEqual(r.status_code, 200)

    # def test_post_status(self):
        # self.__login()
        # r = self.client.post(reverse('login-status:add_status'), data={'message':'msg'})
        # self.assertTrue(Status.objects.filter(message='msg'))

    # def test_delete_status(self):
        # self.__login()
        # Status(pk=90, message='aa', account=self.__get_dummy_profile()).save()
        # r = self.client.get(reverse('login-status:delete_status', args=('90', )))
        # self.assertFalse(Status.objects.filter(pk=90))

    # def test_update_status_url_is_exist(self):
        # response = Client().get('/login-status/')
        # self.assertEqual(response.status_code, 200)

    # def test_update_status_using_index_func(self):
        # found = resolve('/login-status/')
        # self.assertEqual(found.func, index)

    # def test_session_access_update_directly_fail(self):
        # response = Client().get('/login-status/status')
        # self.assertEqual(response.status_code, 302)

    # def test_session_login_with_get_fail(self):
        # response = Client().get('/login-status/custom_auth/login/')
        # self.assertEqual(response.status_code, 302)

    # def test_session_login_wrong_username(self):
        # data = {
            # 'username': 'username',
            # 'password': 'password'
        # }
        # response = Client().post('/login-status/custom_auth/login/', data=data)
        # self.assertEqual(response.status_code, 302)

    # def test_form_status_has_placeholder_and_css_classes(self):
        # form = Status_Form()
        # self.assertIn('class="form-control"', form.as_p())

    # def test_form_validation_for_blank_items(self):
        # form = Status_Form(data={'name': '', 'message': ''})
        # self.assertFalse(form.is_valid())
        # self.assertEqual(
            # form.errors['message'],
            # ["This field is required."]
        # )

    # def test_update_status_post_fail(self):
        # response = Client().post('/login-status/add_status', {'name': 'Team 7', 'message': ''})
        # self.assertEqual(response.status_code, 302)

    # def __login(self):
        # session = self.client.session
        # npm = self.__get_dummy_profile().npm
        # session['kode_identitas'] = npm
        # session.save()
        # return session

    # def __get_dummy_profile(self):
        # profile = Profile(npm='1606894534') if not get_profile_by_npm('1606894534') else get_profile_by_npm('1606894534')
        # profile.save()
        # return profile

# class CsuiApiUnitTest(TestCase):

    # def setUp(self):
        # super().setUp()
        # # mock dependency for faster testing: requests.get
        # self.original_get_request = csui.requests.get
        # self.original_post_request = csui.requests.post
        # class FakeResponse:
            # def json(self):
                # return {'access_token':'123qwerty'}
        # def fake_get_request(url, params=None):
            # return FakeResponse()
        # def fake_post_request(url, data=None, params=None, headers=None):
            # return FakeResponse()
        # csui.requests.get = fake_get_request
        # csui.requests.post = fake_post_request

    # def tearDown(self):
        # csui.requests.get = self.original_get_request
        # csui.requests.post = self.original_post_request

    # def test_get_access_token_success(self):
        # self.assertTrue(csui.get_access_token('uname', 'pass'))

    # def test_get_access_token_fail(self):
        # self.assertFalse(csui.get_access_token(None, None))

    # def test_get_client_id(self):
        # self.assertTrue(csui.get_client_id())

    # def test_verify_user(self):
        # self.assertTrue(csui.verify_user('dfq31x'))

    # def test_get_data_user(self):
        # self.assertTrue(csui.get_data_user('3124f', '134fd'))

# class CustomAuthUnitTest(TestCase):

    # def setUp(self):
        # super().setUp()
        # # mock dependency for faster testing: requests.get
        # self.original_get_access_token = custom_auth.get_access_token
        # def fake_get_access_token(user, password):
            # if user and password:
                # return True
        # custom_auth.get_access_token = fake_get_access_token
        # self.original_verify_user = custom_auth.verify_user
        # def fake_verify_user(access_token):
            # return {'identity_number':'123', 'role':'superuser'}
        # custom_auth.verify_user = fake_verify_user

    # def tearDown(self):
        # custom_auth.get_access_token = self.original_get_access_token
        # custom_auth.verify_user = self.original_verify_user

    # def test_post_login_success(self):
        # response = self.client.post(reverse('login-status:auth_login'), {'username':'utest', 'password':'ptest'})
        # self.assertEqual(response.status_code, 302) # redirected
        # self.assertEqual(response.url, reverse('login-status:index'))
        # self.assertIn('user_login', self.client.session)

    # def test_post_login_fail(self):
        # response = self.client.post(reverse('login-status:auth_login'), {'username':'', 'password':''})
        # self.assertEqual(response.status_code, 302) # redirected
        # self.assertEqual(response.url, reverse('login-status:index'))
        # self.assertNotIn('user_login', self.client.session)

    # def test_logout(self):
        # session = self.client.session
        # session['user_login'] = 'a'
        # session.save()
        # response = self.client.post(reverse('login-status:auth_logout'), {'username':'', 'password':''})
        # self.assertEqual(response.status_code, 302) # redirected
        # self.assertEqual(response.url, reverse('login-status:index'))
        # self.assertNotIn('user_login', self.client.session)


# # Create your tests here.
