from django.conf.urls import url
from .views import delete_status, add_status, index, status
from .custom_auth import auth_login, auth_logout

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^status$', status, name='status'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^delete_status/(?P<id>[0-9999]+)/$', delete_status, name='delete_status'),

    # custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),


]
