from django.http import HttpResponseRedirect
from django.urls import reverse

def get_logged_in_user_npm(request):
    '''
    Semua app akan memakai function ini,
    return NPM jika user sudah login, jika tidak return None
    usage:
    npm = get_logged_in_user_npm()
    if npm: # LOGGED IN!
    else: # belum login!
    jadi
    '''
    return request.session['kode_identitas'] if 'kode_identitas' in request.session else None


def require_login(function):
    ''''
    jadi teman2 ini cara pake nya

    @require_login
    def profile(request):
        #something
    '''
    def wrapper(a1=None, a2=None, *args, **kwargs):
        slf = a1
        request = a2 if a2 else a1
        if get_logged_in_user_npm(request):
            return function(request, *args, **kwargs) if not a2 else function(slf, request, *args, **kwargs)
        else:
            return HttpResponseRedirect(reverse('login-status:index'))
    return wrapper
