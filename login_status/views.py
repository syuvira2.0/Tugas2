import datetime
from django.urls import reverse
from profile_page import render_with_profile as render
from django.shortcuts import render as render_ori
from django.http import HttpResponseRedirect
from .forms import Status_Form
from django.utils import timezone
from .utils import require_login
from profile_page import get_profile_by_npm
from login_status.utils import get_logged_in_user_npm
from profile_page.models import Status

import pytz

# Create your views here.
response = {'status_form': Status_Form, 'logged_in': False, 'status_page': True}


def index(request):
    if 'kode_identitas' in request.session:
        return HttpResponseRedirect(reverse('index'))
    else:
        html = 'login_status/login.html'

    return render_ori(request, html, response)


@require_login
def status(request):
    html = 'update_status.html'
    kode_identitas = request.session['kode_identitas']
    npm = get_logged_in_user_npm(request)
    account = get_profile_by_npm(npm)
    allStatus = Status.objects.filter(account=account)
    response['allStatus'] = allStatus
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        npm = get_logged_in_user_npm(request)
        account = get_profile_by_npm(npm)
        status = Status(account=account, message=request.POST['message'])
        status.save()
        allStatus = Status.objects.filter(account=account)
        response['allStatus'] = allStatus
        html = 'update_status.html'
        return HttpResponseRedirect(reverse('login-status:status'))
    else:
        return HttpResponseRedirect(reverse('login-status:status'))


def delete_status(request, id):
    status_target = Status.objects.all().get(pk=id)
    status_target.delete()
    return HttpResponseRedirect(reverse('login-status:status'))

# ======================================================================== #
