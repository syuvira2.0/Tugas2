"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
# from django.views.generic import RedirectView
from django.views.generic.base import RedirectView
import login_status.urls as login_status
import profile_page.urls as profile
from django.views.generic import RedirectView
import Mencari_Mahasiswa.urls as Mencari_Mahasiswa
import riwayat_page.urls as riwayat

urlpatterns = [
    url(r'^admin/', admin.site.urls),
	 url(r'^login-status/',include(login_status, namespace='login-status')),
    url(r'^profile/',include(profile, namespace='profile')),
	url(r'^$', RedirectView.as_view(url='profile/', permanent='true'), name='index'),
	url(r'^logout/$', login_status.auth_logout, name='logout'),
    # url(r'^halaman-profile/', include(halaman_profile, namespace='halaman-profile')),
    url(r'^Mencari-Mahasiswa/', include(Mencari_Mahasiswa, namespace='Mencari-Mahasiswa')),
	url(r'^riwayat/',include(riwayat, namespace='riwayat')),
]


