from django.shortcuts import render
from login_status.utils import get_logged_in_user_npm

def use_profile(function):
    def wrapper(*args, **kwargs):
        global Profile
        if 'Profile' not in globals():
            from .models import Profile
        return function(*args, **kwargs)
    return wrapper

def use_skill(function):
    def wrapper(*args, **kwargs):
        global Skill
        if 'Skill' not in globals():
            from .models import Skill
        return function(*args, **kwargs)
    return wrapper

def use_status(function):
    def wrapper(*args, **kwargs):
        global Status
        if 'Status' not in globals():
            from .models import Status
        return function(*args, **kwargs)
    return wrapper

@use_status
@use_skill
def get_profile_render_data(npm):
    '''
    return dict yang berisi data yang perlu ditambah 
    ke response dict agar webpage dapat merender profile
    Struktur key set dictionary result:
    profile = {
        name, picture_url, headline, email, linkedin, npm, angkatan, is_complete, skills (list)
    }
    Struktur key set skills:
    skills = {
        id (database primarykey),
        title,
        level
    }
    '''
    profile = get_profile_by_npm(npm)
    profile_status = Status.objects.filter(account=profile).order_by('-id')
    status_count = profile_status.count()
    data = {'name': profile.name, 'picture_url': profile.picture_url, 'headline': profile.headline, 'email': profile.email,
            'linkedin': profile.linkedin, 'npm': profile.npm, 'angkatan': profile.get_angkatan(), 
            'status_count': status_count, 'latest_status': profile_status[0].message if status_count else "No status"}
    skills = [{'id':skill.id, 'title':skill.skill_title, 'level':skill.get_skill_level_display()} for skill in Skill.objects.filter(profile=profile)]
    for key in data:
        if not data[key]:
            data[key] = 'None'
    data['is_complete'] = profile.is_connected_to_linkedin()
    data['skills'] = skills if skills else False
    return data

def render_with_profile(request, template_name, context={}, content_type=None, status=None, using=None):
    context['profile'] = get_profile_render_data(get_logged_in_user_npm(request))
    return render(request, template_name, context, content_type, status, using)

@use_profile
def get_profile_by_npm(npm):
    '''
    return model object Profile dari database, akan return None bila tidak ada di db
    usage:
    profile = get_profile_by_npm(npm)
    if profile: # PROFILE ADA
    else: # Profile tidak ada
    '''
    query_result = Profile.objects.filter(npm=npm)
    if query_result:
        return query_result[0]

@use_profile
def get_list_of_profile_by_name(name):
    '''
    return list of model object Profile dari database berdasarkan substring nama, akan return None bila tidak ada sama sekali di db
    usage:
    profiles = get_list_of_profile_by_name(name)
    if profiles: # PROFILE ADA
    else: # Profile tidak ada
    '''
    return Profile.objects.filter(name__icontains=name)

@use_profile
def should_profile_shows_score(npm):
    return get_profile_by_npm(npm).showing_score
