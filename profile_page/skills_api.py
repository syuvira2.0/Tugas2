import json
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest, Http404
from django.views.generic.base import TemplateView
from login_status.utils import require_login, get_logged_in_user_npm
from profile_page import get_profile_by_npm
from .models import Skill

class SkillAPI(TemplateView):

    @require_login
    def post(self, request, pk=None, *args, **kwargs):
        '''
        Example:
        $.ajax({
            url: 'api/skills/', method: 'POST',
            data: {
                title:'a', 
                level:'A', 
                csrfmiddlewaretoken: 'HvrcQdTtZWWMQc1H70tnagssJ90hPd3UCWFpVd8aYhP7Iz1nuuAeui7m1YT5Sv61'
            },
            success: response => console.log(response),
            error: error => console.log(error)
        })
        '''
        title = request.POST['title']
        level = request.POST['level']
        npm = get_logged_in_user_npm(request)
        profile = get_profile_by_npm(npm)
        skill = Skill(profile=profile, skill_title=title, skill_level=level)
        skill.save()
        return JsonResponse({'id': skill.id}, status=201)

    @require_login
    def delete(self, request, pk=None, *args, **kwargs):
        '''
        Example:
        $.ajax({
            url: 'api/skills/4/', 
            method: 'DELETE', 
            success: response => console.log(response),
            error: error => console.log(error),
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", 'HvrcQdTtZWWMQc1H70tnagssJ90hPd3UCWFpVd8aYhP7Iz1nuuAeui7m1YT5Sv61');
            },
        })
        '''
        query_result = Skill.objects.filter(pk=pk)
        if query_result:
            query_result[0].delete()
            return HttpResponse(status=200)
        else: raise Http404()