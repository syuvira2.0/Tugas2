from django.test import TestCase
# from django.http import HttpRequest
# from django.urls import reverse
# from profile_page import get_profile_by_npm, get_list_of_profile_by_name, \
                            # get_profile_render_data, should_profile_shows_score
# from .models import Profile, Skill

# # Create your tests here.
# class ProfileViewUnitTest(TestCase):

    # def test_render_profile(self):
        # self.__login()
        # profile = self.__get_dummy_profile()
        # profile.name = 'aaa'
        # profile.save()
        # response = self.client.get(reverse('profile:index'))
        # self.assertEqual(response.status_code, 200)
        # html_response = response.content.decode('utf8')
        # self.assertIn(profile.name, html_response)

    # def test_new_profile(self):
        # session = self.__login()
        # npm = '123'
        # session['kode_identitas'] = npm
        # session.save()
        # response = self.client.get(reverse('profile:index'))
        # self.assertEqual(response.status_code, 200)
        # self.assertTrue(Profile.objects.filter(npm=npm))

    # def test_post_linkedin_data(self):
        # name = 'Yudhistira Erlandinata'
        # linkedin = 'https://a.a'
        # pic = 'pic'
        # headline = 'hea'
        # email = 'em'
        # self.__login()
        # response = self.client.post(reverse('profile:linkedin'), data={'name': name, 'url': linkedin, 'pic': pic, 'headline': headline, 'email': email})
        # self.assertEqual(response.status_code, 201)
        # profile = self.__get_dummy_profile()
        # self.assertEqual(name, profile.name)
        # self.assertEqual(linkedin, profile.linkedin)
        # self.assertEqual(pic, profile.picture_url)
        # self.assertEqual(headline, profile.headline)
        # self.assertEqual(email, profile.email)
        # self.assertTrue(profile.is_connected_to_linkedin())

    # def test_post_show_nilai(self):
        # self.__login()
        # profile = self.__get_dummy_profile()
        # profile.showing_score = False
        # profile.save()
        # response = self.client.post(reverse('profile:nilai'), {'show': '1'})
        # self.assertEqual(response.status_code, 201)
        # profile = self.__get_dummy_profile()
        # self.assertTrue(profile.showing_score)

    # def test_post_hide_nilai(self):
        # self.__login()
        # response = self.client.post(reverse('profile:nilai'), {'show': '0'})
        # self.assertEqual(response.status_code, 201)
        # profile = self.__get_dummy_profile()
        # self.assertFalse(profile.showing_score)

    # def __login(self):
        # session = self.client.session
        # npm = self.__get_dummy_profile().npm
        # session['kode_identitas'] = npm
        # session.save()
        # return session

    # def __get_dummy_profile(self):
        # profile = Profile(npm='1606894534') if not get_profile_by_npm('1606894534') else get_profile_by_npm('1606894534')
        # profile.save()
        # return profile


# class DataUnitTest(TestCase):

    # def test_profile_angkatan(self):
        # for i in range(100, 170):
            # npm = str(i)
            # profile = Profile(npm=npm)
            # self.assertEqual('20' + npm[:2], profile.get_angkatan())

    # def test_profile_representation(self):
        # npm = '123'
        # name = 'aaa'
        # email = 'bbb'
        # profile = Profile(npm=npm, name=name, email=email)
        # self.assertEqual('{} ({}): {}'.format(name, npm, email), str(profile))

    # def test_profile_is_connected_linkedin(self):
        # profile = Profile(npm='1', linkedin='a')
        # self.assertTrue(profile.is_connected_to_linkedin())

    # def test_profile_is_not_connected_linkedin(self):
        # profile = Profile(npm='1')
        # self.assertFalse(profile.is_connected_to_linkedin())

    # def test_skill_representation(self):
        # npm = '123'
        # name = 'aaa'
        # profile = Profile(npm=npm, name=name)
        # java = 'Java'
        # skill = Skill(profile=profile, skill_title=java, skill_level=Skill.ADVANCED)
        # self.assertEqual('{} ({}): {}'.format(java, 'Advanced', name), str(skill))

    # def test_get_profile_by_npm(self):
        # npm = '123'
        # profile = Profile(npm=npm)
        # profile.save()
        # self.assertEqual(profile, get_profile_by_npm(npm))

    # def test_get_profile_list_by_name(self):
        # names = ['aa', 'ab', 'ca']
        # for name in names: Profile(npm=name, name=name).save()
        # search_result = get_list_of_profile_by_name('a')
        # search_result_names = [profile.name for profile in search_result]
        # for name in names: self.assertIn(name, search_result_names)

    # def test_get_profile_render_data(self):
        # npm = '123'
        # name = 'aaa'
        # pic_url = 'url'
        # profile = Profile(npm=npm, name=name, picture_url=pic_url)
        # profile.save()
        # profile_render_dict = get_profile_render_data(npm)
        # self.assertIn('name', profile_render_dict)
        # self.assertIn('picture_url', profile_render_dict)
        # self.assertEqual(name, profile_render_dict['name'])
        # self.assertEqual(pic_url, profile_render_dict['picture_url'])

    # def test_profile_score_showing(self):
        # npm = '123'
        # profile = Profile(npm=npm, showing_score=True)
        # profile.save()
        # self.assertTrue(should_profile_shows_score(npm))
        

    # def test_profile_score_not_showing(self):
        # npm = '123'
        # profile = Profile(npm=npm, showing_score=False)
        # profile.save()
        # self.assertFalse(should_profile_shows_score(npm))
        
# class SkillsApiUnitTest(TestCase):

    # def test_post_skill(self):
        # self.__login()
        # title = 'c++'
        # level = 'A'
        # response = self.client.post(reverse('profile:skills'), {'title':title, 'level':level})
        # self.assertEqual(response.status_code, 201)
        # self.assertTrue(Skill.objects.filter(skill_title=title, skill_level=level))

    # def test_delete_skill(self):
        # self.__login()
        # pk = '1'
        # Skill(pk=1, profile=self.__get_dummy_profile()).save()
        # response = self.client.delete(reverse('profile:skills', args=('1', )))
        # self.assertEqual(response.status_code, 200)
        # self.assertFalse(Skill.objects.filter(pk=pk))

    # def test_delete_skill_not_exists(self):
        # self.__login()
        # pk = '1'
        # Skill(pk=1, profile=self.__get_dummy_profile()).save()
        # response = self.client.delete(reverse('profile:skills', args=('17', )))
        # self.assertEqual(response.status_code, 404)
        # self.assertTrue(Skill.objects.filter(pk=pk))

    # def __login(self):
        # session = self.client.session
        # npm = self.__get_dummy_profile().npm
        # session['kode_identitas'] = npm
        # session.save()
        # return session

    # def __get_dummy_profile(self):
        # profile = Profile(npm='1606894534', name='Yudhistira Erlandinata') if not get_profile_by_npm('1606894534') else get_profile_by_npm('1606894534')
        # profile.save()
        # return profile