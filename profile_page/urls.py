from django.conf.urls import url
from .views import index, post_linkedin_data, toggle_nilai
from .skills_api import SkillAPI

urlpatterns = (
    url(r'^$', index, name='index'),
    url(r'^linkedin/$', post_linkedin_data, name='linkedin'),
    url(r'^nilai/$', toggle_nilai, name='nilai'),
    url(r'^api/skills(?:/(?P<pk>[0-9]+))?/$', SkillAPI.as_view(), name='skills'), # POST, DELETE
)