from login_status.utils import require_login, get_logged_in_user_npm
from profile_page import render_with_profile, get_profile_by_npm, should_profile_shows_score
from django.http import HttpResponse, HttpResponseBadRequest
from .models import Profile, Status

# Create your views here.

TEMPLATE = 'profile/profile.html'

@require_login
def index(request):
    check_profile(request)
    return render_with_profile(request, TEMPLATE, {
        'profile_page': True, 
        'editing':True if 'edit' in request.GET and request.GET['edit'] else False,
        'showing_score': should_profile_shows_score(get_logged_in_user_npm(request))
    })

def check_profile(request):
    '''
    Akan membuat profile baru apabila belum ada di DB
    '''
    npm = get_logged_in_user_npm(request)
    if not get_profile_by_npm(npm):
        profile = Profile(npm=npm)
        profile.save()
    
@require_login
def post_linkedin_data(request):
    if request.method == 'POST':
        profile = get_profile_by_npm(get_logged_in_user_npm(request))
        profile.name = request.POST['name']
        profile.linkedin = request.POST['url']
        profile.picture_url = request.POST['pic'] if 'pic' in request.POST else None
        profile.email = request.POST['email'] if 'email' in request.POST else None
        profile.headline = request.POST['headline'] if 'headline' in request.POST else None
        profile.save()
        return HttpResponse(status=201)

@require_login
def toggle_nilai(request):
    if request.method == 'POST':
        profile = get_profile_by_npm(get_logged_in_user_npm(request))
        if request.POST['show'] == '1':
            profile.showing_score = True
        else:
            profile.showing_score = False
        profile.save()
        return HttpResponse(status=201)