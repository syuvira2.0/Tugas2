from django.apps import AppConfig


class RiwayatPageConfig(AppConfig):
    name = 'riwayat_page'
